import React, { Component } from 'react';

export default class Counter extends Component {
    render() {
        const { value } = this.props;
        return (
            <div className="counter">
                <b>{value}</b>
                <div className="counter-controls">
                    <button className="button is-danger is-small" onClick={e=>this.props.onDecrement(1,this.props.id)}>-</button>
                    <button className="button is-success is-small" onClick={e=>this.props.onIncrement(1,this.props.id)}>+</button>
                </div>
            </div>
        );
    }
}