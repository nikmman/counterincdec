import React, { Component } from 'react';

export default class CopyCounter extends Component {
    render() {
        const { value } = this.props;
        return (
            <div className="counter">
                <b>{value}</b>
                <div className="counter-controls">
                    <button className="button is-danger is-small">-</button>
                    <button className="button is-success is-small">+</button>
                </div>
            </div>
        );
    }
}