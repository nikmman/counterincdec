import React, { Component } from 'react';

export default class Total extends Component {
    render() {
        let total = 0;
        this.props.data.forEach((data,i)=> {
           total += data.value;
        });

        return (
            <div style={{marginTop: "5px", display: "flex", alignItems: "center", fontWeight: 700}}>
                <div style={{marginRight: "10px"}}>Total :</div>
                <div>{total}</div>
            </div>
        );
    }
}