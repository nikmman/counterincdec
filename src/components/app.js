import React, { Component } from 'react';
import Counter from './Counter';
import Total from './Total';
import CopyCounter from './CopyCounter';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
          data: [
              { id: 1, value: 0 },
              { id: 2, value: 4 },
              { id: 3, value: 78 },
              { id: 4, value: 3 }
          ]
        };
        this.onIncrement = this.onIncrement.bind(this);
        this.onDecrement = this.onDecrement.bind(this);
    }

    onIncrement(incValue,id) {
      let stateClone = [...this.state.data];
      stateClone.map((data,i)=> {
        if(data.id == id) {
          stateClone[i].value = stateClone[i].value+incValue;
        }
      });
      this.setState({data:stateClone});
    }

    onDecrement(decValue,id) {
        let stateClone = [...this.state.data];
        stateClone.map((data,i)=> {
            if(data.id == id) {
                stateClone[i].value = stateClone[i].value-decValue;
            }
        });
        this.setState({data:stateClone});
    }

    render() {
        return (
            <div>

                <div style={{marginTop: "30px", marginBottom: "10px"}}>Counter</div>

                {this.state.data.map(counter => (
                    <Counter
                        key={counter.id}
                        id={counter.id}
                        value={counter.value}
                        onIncrement={this.onIncrement}
                        onDecrement={this.onDecrement}
                    />
                ))}

                <Total data={this.state.data}/>

                <div style={{marginTop: "30px", marginBottom: "10px"}}>Copy Counter</div>
                {this.state.data.map(counter => (
                    <CopyCounter key={counter.id}
                                 value={counter.value}
                    />
                ))}

            </div>
        );
    }
}
